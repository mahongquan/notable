"use strict";
/* IMPORT */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var path = require("path");
var url_1 = require("url");
var environment_1 = require("../../common/environment");
var window_1 = require("./window");
/* ROUTE */
var Route = /** @class */ (function (_super) {
    __extends(Route, _super);
    function Route() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /* API */
    Route.prototype.load = function () {
        var route = this.name;
        // if (environment_1.default.isDevelopment) {
        //     console.log("use http");
        //     var _a = environment_1.default.wds, protocol = _a.protocol, hostname = _a.hostname, port = _a.port;
        //     this.win.loadURL(protocol + "://" + hostname + ":" + port + "?route=" + route);
        // }
        // else {
            console.log(path.join(__dirname, '/../../../index.html'));
            this.win.loadURL(url_1.format({
                pathname: path.join(__dirname, '/../../../index.html'),
                protocol: 'file',
                slashes: true,
                query: {
                    route: route
                }
            }));
        // }
    };
    return Route;
}(window_1.default));
/* EXPORT */
exports.default = Route;
