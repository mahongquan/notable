"use strict";
/* IMPORT */
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("lodash");
var path = require("path");
var electron_1 = require("electron");
var is = require("electron-is");
var windowStateKeeper = require("electron-window-state");
var environment_1 = require("../../common/environment");
/* WINDOW */
var Window = /** @class */ (function () {
    /* CONSTRUCTOR */
    function Window(name, options, stateOptions) {
        if (options === void 0) { options = {}; }
        if (stateOptions === void 0) { stateOptions = {}; }
        this.name = name;
        this.options = options;
        this.stateOptions = stateOptions;
        this.init();
        this.events();
    }
    /* SPECIAL */
    Window.prototype.init = function () {
        this.initWindow();
        this.initDebug();
        this.initLocalShortcuts();
        this.initMenu();
        this.load();
    };
    Window.prototype.initWindow = function () {
        this.win = this.make();
    };
    Window.prototype.initDebug = function () {
        var _this = this;
        if (!environment_1.default.isDevelopment)
            return;
        this.win.webContents.openDevTools();
        this.win.webContents.on('devtools-opened', function () {
            _this.win.focus();
            setImmediate(function () { return _this.win.focus(); });
        });
    };
    Window.prototype.initMenu = function () { };
    Window.prototype.initLocalShortcuts = function () { };
    Window.prototype.events = function () {
        this.___readyToShow();
        this.___closed();
    };
    /* READY TO SHOW */
    Window.prototype.___readyToShow = function () {
        this.win.on('ready-to-show', this.__readyToShow.bind(this));
    };
    Window.prototype.__readyToShow = function () {
        this.win.show();
        this.win.focus();
    };
    /* CLOSED */
    Window.prototype.___closed = function () {
        this.win.on('closed', this.__closed.bind(this));
    };
    Window.prototype.__closed = function () {
        delete this.win;
    };
    /* API */
    Window.prototype.make = function (id, options, stateOptions) {
        if (id === void 0) { id = this.name; }
        if (options === void 0) { options = this.options; }
        if (stateOptions === void 0) { stateOptions = this.stateOptions; }
        stateOptions = _.merge({
            file: id + ".json",
            defaultWidth: 800,
            defaultHeight: 800
        }, stateOptions);
        var state = windowStateKeeper(stateOptions), dimensions = _.pick(state, ['x', 'y', 'width', 'height']);
        options = _.merge(dimensions, {
            frame: !is.macOS(),
            backgroundColor: '#fdfdfd',
            icon: path.join(".", 'images', "icon." + (is.windows() ? 'ico' : 'png')),
            show: false,
            title: electron_1.app.getName(),
            titleBarStyle: 'hiddenInset',
            webPreferences: {
                 contextIsolation: false,
                  nodeIntegration:true,
                  enableRemoteModule: true
            }
        }, options);
        var win = new electron_1.BrowserWindow(options);
        state.manage(win);
        return win;
    };
    Window.prototype.load = function () { };
    return Window;
}());
/* EXPORT */
exports.default = Window;
