"use strict";
/* IMPORT */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var is = require("electron-is");
var package_json_1 ={productName:"",version:"",license:"",author:""};
var environment_1 = require("../../common/environment");
var menu_1 = require("../utils/menu");
var route_1 = require("./route");
/* MAIN */
var Main = /** @class */ (function (_super) {
    __extends(Main, _super);
    /* CONSTRUCTOR */
    function Main(name, options, stateOptions) {
        if (name === void 0) { name = 'main'; }
        if (options === void 0) { options = { minWidth: 685, minHeight: 425 }; }
        if (stateOptions === void 0) { stateOptions = { defaultWidth: 850, defaultHeight: 525 }; }
        return _super.call(this, name, options, stateOptions) || this;
    }
    /* SPECIAL */
    Main.prototype.initLocalShortcuts = function () { };
    Main.prototype.initMenu = function (flags) {
        var _this = this;
        if (flags === void 0) { flags = false; }
        var template = menu_1.default.filterTemplate([
            {
                label: electron_1.app.getName(),
                submenu: [
                    {
                        role: 'about',
                        visible: is.macOS()
                    },
                    {
                        type: 'separator'
                    },
                    {
                        label: 'Import...',
                        click: function () { return _this.win.webContents.send('import'); }
                    },
                    {
                        type: 'separator'
                    },
                    {
                        label: 'Open Data Directory',
                        click: function () { return _this.win.webContents.send('cwd-open-in-app'); }
                    },
                    {
                        label: 'Change Data Directory...',
                        click: function () { return _this.win.webContents.send('cwd-change'); }
                    },
                    {
                        type: 'separator',
                        visible: is.macOS()
                    },
                    {
                        role: 'services',
                        submenu: [],
                        visible: is.macOS()
                    },
                    {
                        type: 'separator',
                        visible: is.macOS()
                    },
                    {
                        role: 'hide',
                        visible: is.macOS()
                    },
                    {
                        role: 'hideothers',
                        visible: is.macOS()
                    },
                    {
                        role: 'unhide',
                        visible: is.macOS()
                    },
                    {
                        type: 'separator',
                        visible: is.macOS()
                    },
                    { role: 'quit' }
                ]
            },
            {
                label: 'Note',
                submenu: [
                    {
                        label: 'New',
                        accelerator: 'CommandOrControl+N',
                        enabled: flags && !flags.isMultiEditorEditing,
                        click: function () { return _this.win.webContents.send('note-new'); }
                    },
                    {
                        label: 'Duplicate',
                        accelerator: 'CommandOrControl+Shift+N',
                        enabled: flags && flags.hasNote && !flags.isMultiEditorEditing,
                        click: function () { return _this.win.webContents.send('note-duplicate'); }
                    },
                    {
                        type: 'separator'
                    },
                    {
                        label: 'Open in Default App',
                        accelerator: 'CommandOrControl+O',
                        enabled: flags && flags.hasNote && !flags.isMultiEditorEditing,
                        click: function () { return _this.win.webContents.send('note-open-in-app'); }
                    },
                    {
                        label: "Reveal in " + (is.macOS() ? 'Finder' : 'Folder'),
                        accelerator: 'CommandOrControl+Alt+R',
                        enabled: flags && flags.hasNote && !flags.isMultiEditorEditing,
                        click: function () { return _this.win.webContents.send('note-reveal'); }
                    },
                    {
                        type: 'separator'
                    },
                    {
                        label: flags && flags.hasNote && flags.isEditorEditing ? 'Stop Editing' : 'Edit',
                        accelerator: 'CommandOrControl+E',
                        enabled: flags && flags.hasNote && !flags.isMultiEditorEditing,
                        click: function () { return _this.win.webContents.send('note-edit-toggle'); }
                    },
                    {
                        label: flags && flags.hasNote && flags.isTagsEditing ? 'Stop Editing Tags' : 'Edit Tags',
                        accelerator: 'CommandOrControl+Shift+T',
                        enabled: flags && flags.hasNote && !flags.isMultiEditorEditing,
                        click: function () { return _this.win.webContents.send('note-edit-tags-toggle'); }
                    },
                    {
                        label: flags && flags.hasNote && flags.isAttachmentsEditing ? 'Stop Editing Attachments' : 'Edit Attachments',
                        accelerator: 'CommandOrControl+Shift+A',
                        enabled: flags && flags.hasNote && !flags.isMultiEditorEditing,
                        click: function () { return _this.win.webContents.send('note-edit-attachments-toggle'); }
                    },
                    {
                        type: 'separator'
                    },
                    {
                        label: flags && flags.hasNote && flags.isNoteFavorited ? 'Unfavorite' : 'Favorite',
                        accelerator: 'CommandOrControl+D',
                        enabled: flags && flags.hasNote && !flags.isMultiEditorEditing,
                        click: function () { return _this.win.webContents.send('note-favorite-toggle'); }
                    },
                    {
                        label: flags && flags.hasNote && flags.isNotePinned ? 'Unpin' : 'Pin',
                        accelerator: 'CommandOrControl+P',
                        enabled: flags && flags.hasNote && !flags.isMultiEditorEditing,
                        click: function () { return _this.win.webContents.send('note-pin-toggle'); }
                    },
                    {
                        type: 'separator'
                    },
                    {
                        label: 'Move to Trash',
                        accelerator: 'CommandOrControl+Backspace',
                        enabled: flags && flags.hasNote && !flags.isNoteDeleted && !flags.isMultiEditorEditing,
                        visible: flags && flags.hasNote && !flags.isNoteDeleted && !flags.isEditorEditing,
                        click: function () { return _this.win.webContents.send('note-move-to-trash'); }
                    },
                    {
                        label: 'Move to Trash',
                        accelerator: 'CommandOrControl+Alt+Backspace',
                        enabled: flags && flags.hasNote && !flags.isNoteDeleted && !flags.isMultiEditorEditing,
                        visible: flags && flags.hasNote && !flags.isNoteDeleted && flags.isEditorEditing,
                        click: function () { return _this.win.webContents.send('note-move-to-trash'); }
                    },
                    {
                        label: 'Restore',
                        accelerator: 'CommandOrControl+Shift+Backspace',
                        enabled: flags && flags.hasNote && flags.isNoteDeleted && !flags.isMultiEditorEditing,
                        visible: flags && flags.hasNote && flags.isNoteDeleted,
                        click: function () { return _this.win.webContents.send('note-restore'); }
                    },
                    {
                        label: 'Permanently Delete',
                        enabled: flags && flags.hasNote && !flags.isMultiEditorEditing,
                        visible: flags && flags.hasNote,
                        click: function () { return _this.win.webContents.send('note-permanently-delete'); }
                    }
                ]
            },
            {
                label: 'Edit',
                submenu: [
                    // { role: 'undo' },
                    // { role: 'redo' },
                    // { type: 'separator' },
                    { role: 'cut' },
                    { role: 'copy' },
                    { role: 'paste' },
                    { role: 'pasteandmatchstyle' },
                    { role: 'delete' },
                    { role: 'selectall' },
                    {
                        type: 'separator'
                    },
                    {
                        label: 'Select Notes - All',
                        accelerator: 'CommandOrControl+Alt+A',
                        click: function () { return _this.win.webContents.send('multi-editor-select-all'); }
                    },
                    {
                        label: 'Select Notes - Invert',
                        accelerator: 'CommandOrControl+Alt+I',
                        click: function () { return _this.win.webContents.send('multi-editor-select-invert'); }
                    },
                    {
                        label: 'Select Notes - Clear',
                        accelerator: 'CommandOrControl+Alt+C',
                        click: function () { return _this.win.webContents.send('multi-editor-select-clear'); }
                    },
                    {
                        type: 'separator'
                    },
                    {
                        label: 'Empty Trash',
                        click: function () { return _this.win.webContents.send('trash-empty'); }
                    },
                    {
                        type: 'separator',
                        visible: is.macOS()
                    },
                    {
                        label: 'Speech',
                        submenu: [
                            { role: 'startspeaking' },
                            { role: 'stopspeaking' }
                        ],
                        visible: is.macOS()
                    }
                ]
            },
            {
                label: 'View',
                submenu: [
                    {
                        role: 'reload',
                        visible: environment_1.default.isDevelopment
                    },
                    {
                        role: 'forcereload',
                        visible: environment_1.default.isDevelopment
                    },
                    {
                        role: 'toggledevtools',
                        visible: environment_1.default.isDevelopment
                    },
                    {
                        type: 'separator',
                        visible: environment_1.default.isDevelopment
                    },
                    { role: 'resetzoom' },
                    { role: 'zoomin' },
                    { role: 'zoomout' },
                    { type: 'separator' },
                    {
                        label: 'Toggle Focus Mode',
                        accelerator: 'CommandOrControl+Alt+F',
                        click: function () { return _this.win.webContents.send('window-focus-toggle'); }
                    },
                    { role: 'togglefullscreen' }
                ]
            },
            {
                role: 'window',
                submenu: [
                    { role: 'close' },
                    { role: 'minimize' },
                    {
                        role: 'zoom',
                        visible: is.macOS()
                    },
                    {
                        type: 'separator'
                    },
                    {
                        label: 'Search',
                        accelerator: 'CommandOrControl+F',
                        click: function () { return _this.win.webContents.send('search-focus'); }
                    },
                    {
                        type: 'separator'
                    },
                    {
                        label: 'Previous Tag',
                        accelerator: 'Control+Alt+Shift+Tab',
                        click: function () { return _this.win.webContents.send('tag-previous'); }
                    },
                    {
                        label: 'Next Tag',
                        accelerator: 'Control+Alt+Tab',
                        click: function () { return _this.win.webContents.send('tag-next'); }
                    },
                    {
                        type: 'separator'
                    },
                    {
                        label: 'Previous Note',
                        accelerator: 'Control+Shift+Tab',
                        click: function () { return _this.win.webContents.send('search-previous'); }
                    },
                    {
                        label: 'Next Note',
                        accelerator: 'Control+Tab',
                        click: function () { return _this.win.webContents.send('search-next'); }
                    },
                    {
                        type: 'separator',
                        visible: is.macOS()
                    },
                    {
                        role: 'front',
                        visible: is.macOS()
                    }
                ]
            },
            {
                role: 'help',
                submenu: [
                    {
                        label: 'Learn More',
                        click: function () { return electron_1.shell.openExternal(package_json_1.default.homepage); }
                    },
                    {
                        label: 'Tutorial',
                        click: function () { return _this.win.webContents.send('tutorial-dialog'); }
                    },
                    {
                        label: 'Support',
                        click: function () { return electron_1.shell.openExternal(package_json_1.default.bugs.url); }
                    },
                    { type: 'separator' },
                    {
                        label: 'View License',
                        click: function () { return electron_1.shell.openExternal(package_json_1.default.homepage + "/blob/master/LICENSE"); }
                    }
                ]
            }
        ]);
        var menu = electron_1.Menu.buildFromTemplate(template);
        electron_1.Menu.setApplicationMenu(menu);
    };
    Main.prototype.events = function () {
        _super.prototype.events.call(this);
        this.___fullscreenEnter();
        this.___fullscreenLeave();
        this.___flagsUpdate();
        this.___navigateUrl();
    };
    /* FULLSCREEN ENTER */
    Main.prototype.___fullscreenEnter = function () {
        this.win.on('enter-full-screen', this.__fullscreenEnter.bind(this));
    };
    Main.prototype.__fullscreenEnter = function () {
        this.win.webContents.send('window-fullscreen-set', true);
    };
    /* FULLSCREEN LEAVE */
    Main.prototype.___fullscreenLeave = function () {
        this.win.on('leave-full-screen', this.__fullscreenLeave.bind(this));
    };
    Main.prototype.__fullscreenLeave = function () {
        this.win.webContents.send('window-fullscreen-set', false);
    };
    /* FLAGS UPDATE */
    Main.prototype.___flagsUpdate = function () {
        electron_1.ipcMain.on('flags-update', this.__flagsUpdate.bind(this));
    };
    Main.prototype.__flagsUpdate = function (event, flags) {
        this.initMenu(flags);
    };
    /* NAVIGATE URL */
    Main.prototype.___navigateUrl = function () {
        this.win.webContents.on('new-window', this.__navigateUrl.bind(this));
    };
    Main.prototype.__navigateUrl = function (event, url) {
        if (url === this.win.webContents.getURL())
            return;
        event.preventDefault();
        electron_1.shell.openExternal(url);
    };
    return Main;
}(route_1.default));
/* EXPORT */
exports.default = Main;
