"use strict";
/* IMPORT */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var route_1 = require("./route");
/* CWD */
var CWD = /** @class */ (function (_super) {
    __extends(CWD, _super);
    /* CONSTRUCTOR */
    function CWD(name, options, stateOptions) {
        if (name === void 0) { name = 'cwd'; }
        if (options === void 0) { options = { resizable: false, minWidth: 560, minHeight: 470 }; }
        if (stateOptions === void 0) { stateOptions = { defaultWidth: 560, defaultHeight: 470 }; }
        return _super.call(this, name, options, stateOptions) || this;
    }
    return CWD;
}(route_1.default));
/* EXPORT */
exports.default = CWD;
