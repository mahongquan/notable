"use strict";
/* IMPORT */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var electron_updater_1 = require("electron-updater");
var is = require("electron-is");
var fs = require("fs");
var package_json_1 ={productName:"",version:"",license:"",author:""};
var config_1 = require("../common/config");
var environment_1 = require("../common/environment");
var cwd_1 = require("./windows/cwd");
var main_1 = require("./windows/main");
/* APP */
var App = /** @class */ (function () {
    /* CONSTRUCTOR */
    function App() {
        this.init();
        this.events();
    }
    /* SPECIAL */
    App.prototype.init = function () {
        this.initAbout();
        this.initContextMenu();
    };
    App.prototype.initAbout = function () {
        if (!is.macOS())
            return;
        var productName = package_json_1.default.productName, version = package_json_1.default.version, license = package_json_1.default.license, author = package_json_1.default.author;
        electron_1.app.setAboutPanelOptions({
            applicationName: productName,
            applicationVersion: version,
            copyright: license + " \u00A9 " + author.name,
            version: ''
        });
    };
    App.prototype.initContextMenu = function () { };
    App.prototype.initDebug = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (!environment_1.default.isDevelopment)
                    return [2 /*return*/];
                return [2 /*return*/];
            });
        });
    };
    App.prototype.events = function () {
        this.___windowAllClosed();
        this.___activate();
        this.___ready();
        this.___cwdChanged();
    };
    /* WINDOW ALL CLOSED */
    App.prototype.___windowAllClosed = function () {
        electron_1.app.on('window-all-closed', this.__windowAllClosed.bind(this));
    };
    App.prototype.__windowAllClosed = function () {
        if (is.macOS())
            return;
        electron_1.app.quit();
    };
    /* ACTIVATE */
    App.prototype.___activate = function () {
        electron_1.app.on('activate', this.__activate.bind(this));
    };
    App.prototype.__activate = function () {
        if (this.win && this.win.win)
            return;
        this.load();
    };
    /* READY */
    App.prototype.___ready = function () {
        electron_1.app.on('ready', this.__ready.bind(this));
    };
    App.prototype.__ready = function () {
        this.initDebug();
        electron_updater_1.autoUpdater.checkForUpdatesAndNotify();
        this.load();
    };
    /* CWD CHANGED */
    App.prototype.___cwdChanged = function () {
        electron_1.ipcMain.on('cwd-changed', this.__cwdChanged.bind(this));
    };
    App.prototype.__cwdChanged = function () {
        if (this.win)
            this.win.win.close();
        this.load();
    };
    /* API */
    App.prototype.load = function () {
        var cwd = config_1.default.cwd;
        if (cwd && fs.existsSync(cwd)) {
            this.win = new main_1.default();
        }
        else {
            this.win = new cwd_1.default();
        }
    };
    return App;
}());
/* EXPORT */
exports.default = App;
