"use strict";
/* IMPORT */
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("lodash");
/* MENU */
var Menu = {
    filterTemplate: function (template) {
        return _.cloneDeepWith(template, function (val) {
            if (!_.isArray(val))
                return;
            return val.filter(function (ele) { return !_.isObject(ele) || !ele.hasOwnProperty('visible') || ele.visible; }).map(Menu.filterTemplate);
        });
    }
};
/* EXPORT */
exports.default = Menu;
