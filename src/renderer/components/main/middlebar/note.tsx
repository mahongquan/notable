
/* IMPORT */

import * as React from 'react';
import {connect} from 'overstated';
import Markdown from '@renderer/utils/markdown';
import Main from '@renderer/containers/main';
import NoteBadge from './note_badge';

/* NOTE */

const Note = ({ note, style, title, hasAttachments, isActive, isSelected, isDeleted, isFavorited, isPinned, isMultiEditorEditing, set, toggleNote }) => {

  const html = Markdown.render ( title );
  console.log("Note=====")
  console.log(title);
  console.log(html);
  //const onClick = event => Svelto.Keyboard.keystroke.hasCtrlOrCmd ( event ) ? toggleNote ( note ) : set ( note, true );
  const onClick=()=>{};
  return (
    <div style={style} className={`note-button ${!isMultiEditorEditing && isActive ? 'label' : 'button'} ${( isMultiEditorEditing ? isSelected : isActive ) ? 'active' : ''} small fluid compact circular`} data-checksum={note.checksum} data-filepath={note.filePath} data-deleted={isDeleted} data-favorited={isFavorited} onClick={onClick} tabIndex={0}> {/* tabIndex is need in order to have the notes focusable, we use that for navigating with arrow */}
      <span className="title" dangerouslySetInnerHTML={{ __html: html }}></span>
      {!hasAttachments ? null : (
        <NoteBadge icon="paperclip" />
      )}
      {!isFavorited ? null : (
        <NoteBadge icon="star" />
      )}
      {!isPinned ? null : (
        <NoteBadge icon="pin" />
      )}
    </div>
  );

};

/* EXPORT */

export default connect ({
  store: Main,
  selector: ({ store, note, style }) => ({
    note, style,
    title: store.note.getTitle ( note ),
    hasAttachments: !!store.note.getAttachments ( note ).length,
    isActive: ( store.note.get () === note ),
    isSelected: store.multiEditor.isNoteSelected ( note ),
    isDeleted: store.note.isDeleted ( note ),
    isFavorited: store.note.isFavorited ( note ),
    isPinned: store.note.isPinned ( note ),
    isMultiEditorEditing: store.multiEditor.isEditing (),
    set: store.note.set,
    toggleNote: store.multiEditor.toggleNote
  })
})( Note );
