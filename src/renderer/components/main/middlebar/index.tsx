
/* IMPORT */

import * as React from 'react';
import {connect} from 'overstated';
import Main from '@renderer/containers/main';
import Content from './content';
import Header from './header';
import Toolbar from './toolbar';

/* MIDDLEBAR */

const Middlebar = ({ isFocus }) => {

  if ( isFocus ) return null;

  return (
    <div id="middlebar" className="layout">
      <Toolbar />
      <Header />
      <Content />
    </div>
  );

};

/* EXPORT */

export default connect ({
  store: Main,
  selector: ({ store }) => ({
    isFocus: store.window.isFocus ()
  })
})( Middlebar );
