
/* IMPORT */

import {connect} from 'overstated';
import {Component} from 'react-component-renderless';
import Main from '@renderer/containers/main';

/* PREVIEW PLUGINS */

class PreviewPlugins extends Component<{ store: IMain }, undefined> {

  /* SPECIAL */

  componentDidMount () {
    console.log($);
    // $.$document.on ( 'click', '.editor.preview a.note', this.__noteClick );
    // $.$document.on ( 'click', '.editor.preview a.tag', this.__tagClick );

  }

  componentWillUnmount () {

    // $.$document.off ( 'click', this.__noteClick );
    // $.$document.off ( 'click', this.__tagClick );

  }

  /* HANDLERS */

  __noteClick = ( event ) => {

    const filePath = $(event.currentTarget).data ( 'filepath' ),
          note = this.props.store.note.get ( filePath );

    this.props.store.note.set ( note, true );

    return false;

  }

  __tagClick = ( event ) => {

    const tag = $(event.currentTarget).data ( 'tag' );

    this.props.store.tag.set ( tag );

    return false;

  }

}

/* EXPORT */

export default connect ({
  store: Main,
  shouldComponentUpdate: false
})( PreviewPlugins );
