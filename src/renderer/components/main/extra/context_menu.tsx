
/* IMPORT */

import * as _ from 'lodash';
const contextMenu=window.require('electron-context-menu');
const Dialog=window.require('electron-dialog').default;
const is=window.require("electron-is");
import {connect} from 'overstated';
import {Component} from 'react-component-renderless';
import Main from '@renderer/containers/main';

/* CONTEXT MENU */

class ContextMenu extends Component {

  /* VARIABLES */

  ele; attachment; note; tag; // Globals pointing to the current element/attachment/note/tag object

  /* SPECIAL */

  componentDidMount () {

    this.initAttachmentMenu ();
    this.initNoteMenu ();
    this.initNoteTagMenu ();
    this.initTagMenu ();
    this.initTrashMenu ();
    this.initFallbackMenu ();

  }

  /* UTILITIES */

  _getItem ( x, y, selector ) {

    const eles = document.elementsFromPoint ( x, y );

    return $(eles).filter ( selector )[0];

  }

  _makeMenu ( selector: string | Function = '*', items: any[] = [], itemsUpdater = _.noop ) {

    contextMenu ({
      prepend: () => items,
      shouldShowMenu: ( event, { x, y } ) => {

        const ele = _.isString ( selector ) ? this._getItem ( x, y, selector ) : selector ( x, y );

        if ( !ele ) return false;

        this.ele = ele;

        itemsUpdater ( items );

        return true;

      }
    });

  }

  /* INIT */

  initAttachmentMenu () {

    this._makeMenu ( '.attachment', [
      {
        label: 'Open',
        click: () => this.props.store.attachment.openInApp ( this.attachment )
      },
      {
        label: `Reveal in ${is.macOS () ? 'Finder' : 'Folder'}`,
        click: () => this.props.store.attachment.reveal ( this.attachment )
      },
      {
        type: 'separator'
      },
      {
        label: 'Rename',
        click: () => Dialog.alert ( 'Simply rename the actual attachment file while Notable is open' )
      },
      {
        label: 'Delete',
        click: () => this.props.store.note.removeAttachment ( undefined, this.attachment )
      }
    ], this.updateAttachmentMenu.bind ( this ) );

  }

  initNoteMenu () {

    this._makeMenu ( '.note-button, .editor .note', [
      {
        label: 'Duplicate',
        click: () => this.props.store.note.duplicate ( this.note )
      },
      {
        type: 'separator'
      },
      {
        label: 'Open in Default App',
        click: () => this.props.store.note.openInApp ( this.note )
      },
      {
        label: `Reveal in ${is.macOS () ? 'Finder' : 'Folder'}`,
        click: () => this.props.store.note.reveal ( this.note )
      },
      {
        type: 'separator'
      },
      {
        label: 'Favorite',
        click: () => this.props.store.note.toggleFavorite ( this.note, true )
      },
      {
        label: 'Unfavorite',
        click: () => this.props.store.note.toggleFavorite ( this.note, false )
      },
      {
        type: 'separator'
      },
      {
        label: 'Move to Trash',
        click: () => this.props.store.note.toggleDeleted ( this.note, true )
      },
      {
        label: 'Restore',
        click: () => this.props.store.note.toggleDeleted ( this.note, false )
      },
      {
        label: 'Permanently Delete',
        click: () => this.props.store.note.delete ( this.note )
      }
    ], this.updateNoteMenu.bind ( this ) );

  }

  initNoteTagMenu () {

    this._makeMenu ( '.tag:not([data-has-children]):not(a)', [
      {
        label: 'Remove',
        click: () => this.props.store.note.removeTag ( undefined, $(this.ele).text () )
      }
    ]);

  }

  initTagMenu () {

    this._makeMenu ( '.tag[data-has-children="true"]', [
      {
        label: 'Collapse',
        click: () => this.props.store.tag.toggleCollapse ( this.tag, true )
      },
      {
        label: 'Expand',
        click: () => this.props.store.tag.toggleCollapse ( this.tag, false )
      }
    ], this.updateTagMenu.bind ( this ) );

  }

  initTrashMenu () {

    this._makeMenu ( '.tag[title="Trash"]', [
      {
        label: 'Empty Trash',
        click: this.props.store.trash.empty
      }
    ], this.updateTrashMenu.bind ( this ) );

  }

  initFallbackMenu () {

    this._makeMenu ( ( x, y ) => !this._getItem ( x, y, '.attachment, .note-button, .editor .note, .tag:not([data-has-children]), .tag[data-has-children="true"], .tag[title="Trash"]' ) );

  }

  /* UPDATE */

  updateAttachmentMenu ( items ) {

    const fileName = $(this.ele).data ( 'filename' );

    this.attachment = this.props.store.attachment.get ( fileName );

  }

  updateNoteMenu ( items ) {

    const filePath = $(this.ele).data ( 'filepath' );

    this.note = this.props.store.note.get ( filePath );

    const isFavorited = this.props.store.note.isFavorited ( this.note ),
          isDeleted = this.props.store.note.isDeleted ( this.note )

    items[5].visible = !isFavorited;
    items[6].visible = !!isFavorited;
    items[8].visible = !isDeleted;
    items[9].visible = !!isDeleted;

  }

  updateTagMenu ( items ) {

    this.tag = $(this.ele).data ( 'tag' );

    const isCollapsed = this.props.store.tag.isCollapsed ( this.tag );

    items[0].visible = !isCollapsed;
    items[1].visible = isCollapsed;

  }

  updateTrashMenu ( items ) {

    items[0].enabled = !this.props.store.trash.isEmpty ();

  }

}

/* EXPORT */

export default connect ({
  store: Main,
  shouldComponentUpdate: false
})( ContextMenu );
