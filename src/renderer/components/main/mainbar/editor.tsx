
/* IMPORT */

import * as React from 'react';
import {connect} from 'overstated';
import Main from '@renderer/containers/main';
import EditorEditing from './editor_editing';
import EditorEmpty from './editor_empty';
import EditorPreview from './editor_preview';

/* EDITOR */

const Editor = ({ hasNote, isEditing, isLoading }) => {

  if ( isLoading || !hasNote ) return <EditorEmpty />;

  if ( isEditing ) return <EditorEditing />;

  return <EditorPreview />;

};

/* EXPORT */

export default connect ({
  store: Main,
  selector: ({ store }) => ({
    hasNote: !!store.note.get (),
    isEditing: store.editor.isEditing (),
    isLoading: store.loading.get ()
  })
})( Editor );
