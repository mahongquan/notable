
/* IMPORT */

import * as React from 'react';
import {connect} from 'overstated';
import Main from '@renderer/containers/main';
import Button from './multi_editor_button';
import Tagbox from './multi_editor_tagbox';

/* MULTI EDITOR */

const MultiEditor = ({ notesNr, favorite, unfavorite, pin, unpin, trash, untrash, del, tagsAdd, tagsRemove, openInApp }) => (
  <div className="multi-editor">
    <h1>{notesNr} notes selected</h1>
    <div className="store bordered actions">
      <div className="multiple fluid vertical">
        <div className="multiple fluid">
          <div className="multiple fluid joined actions-favorite">
            <Button icon="star_outline" title="Unfavorite" onClick={unfavorite} />
            <Button icon="star" title="Favorite" onClick={favorite} />
          </div>
          <div className="multiple fluid joined actions-pin">
            <Button icon="pin_outline" title="Unpin" onClick={unpin} />
            <Button icon="pin" title="Pin" onClick={pin} />
          </div>
        </div>
        <div className="multiple fluid">
          <div className="multiple fluid joined actions-delete">
            <Button icon="delete" title="Move to Trash" onClick={trash} />
            <Button icon="delete_restore" title="Restore" onClick={untrash} />
            <Button icon="delete_forever" color="red inverted" title="Permanently Delete" onClick={del} />
          </div>
          <Button icon="open_in_new" title="Open in Default App" onClick={openInApp} />
        </div>
        <Tagbox icon="tag_plus" title="Add Tags" placeholder="Add Tag..." onClick={tagsAdd} />
        <Tagbox icon="tag_minus" title="Remove Tags" placeholder="Remove Tag..." onClick={tagsRemove} />
      </div>
    </div>
  </div>
);

/* EXPORT */

export default connect ({
  store: Main,
  selector: ({ store }) => ({
    notesNr: store.multiEditor.getNotes ().length,
    favorite: store.multiEditor.favorite,
    unfavorite: store.multiEditor.unfavorite,
    pin: store.multiEditor.pin,
    unpin: store.multiEditor.unpin,
    trash: store.multiEditor.trash,
    untrash: store.multiEditor.untrash,
    del: store.multiEditor.delete,
    tagsAdd: store.multiEditor.tagsAdd,
    tagsRemove: store.multiEditor.tagsRemove,
    openInApp: store.multiEditor.openInApp
  })
})( MultiEditor );
