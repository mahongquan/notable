
/* IMPORT */

const is=window.require("electron-is");
import * as React from 'react';
import {connect} from 'overstated';
import Main from '@renderer/containers/main';

/* TOOLBAR */

const Toolbar = ({ isFullscreen }) => {

  if ( !is.macOS () || isFullscreen ) return null;

  return <div className="layout-header"></div>;

};

/* EXPORT */

export default connect ({
  store: Main,
  selector: ({ store }) => ({
    isFullscreen: store.window.isFullscreen ()
  })
})( Toolbar );
