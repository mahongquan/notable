"use strict";
/* IMPORT */
Object.defineProperty(exports, "__esModule", { value: true });
var os = require("os");
var Store = require('electron-store');
/* SETTINGS */
var Settings = new Store({
    name: '.notable',
    cwd: os.homedir(),
    defaults: {
        cwd: undefined,
        codemirror: {
            options: {
                lineWrapping: true
            }
        },
        sorting: {
            by: 'title',
            type: 'ascending'
        },
        tutorial: false // Did we import the tutorial yet?
    }
});
/* EXPORT */
exports.default = Settings;
