"use strict";
/* ENVIRONMENT */
Object.defineProperty(exports, "__esModule", { value: true });
var Environment = {
    environment: process.env.NODE_ENV,
    isDevelopment: (process.env.NODE_ENV !== 'production'),
    wds: {
        protocol: 'http',
        hostname: 'localhost',
        port: process.env.ELECTRON_WEBPACK_WDS_PORT
    }
};
/* EXPORT */
exports.default = Environment;
