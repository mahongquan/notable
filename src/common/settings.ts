
/* IMPORT */

// import * as os from 'os';
const os=window.require("os")
let Store=window.require('electron-store');

/* SETTINGS */

const Settings = new Store ({
  name: '.notable',
  cwd: os.homedir (),
  defaults: {
    cwd: undefined,
    codemirror: {
      options: {
        lineWrapping: true
      }
    },
    sorting: {
      by: 'title',
      type: 'ascending'
    },
    tutorial: false // Did we import the tutorial yet?
  }
});

/* EXPORT */

export default Settings;
