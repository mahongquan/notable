"use strict";
/* IMPORT */
Object.defineProperty(exports, "__esModule", { value: true });
var path = require("path");
var settings_1 = require("./settings");
/* CONFIG */
var Config = {
    get cwd() {
        return settings_1.default.get('cwd');
    },
    attachments: {
        get path() {
            var cwd = Config.cwd;
            return cwd ? path.join(cwd, 'attachments') : undefined;
        },
        globs: ['**/*', '!**/.*'],
        re: /attachments(?:\\|\/)(?!\.).*$/,
        token: '@attachment' // Usable in urls
    },
    notes: {
        get path() {
            var cwd = Config.cwd;
            return cwd ? path.join(cwd, 'notes') : undefined;
        },
        globs: ['**/*.{md,mkd,mdwn,mdown,markdown,markdn,mdtxt,mdtext,txt}'],
        re: /\.(?:md|mkd|mdwn|mdown|markdown|markdn|mdtxt|mdtext|txt)$/,
        token: '@note' // Usable in urls
    },
    tags: {
        token: '@tag' // Usable in urls
    },
    search: {
        tokenizer: /\s+/g
    },
    sorting: {
        by: settings_1.default.get('sorting.by'),
        type: settings_1.default.get('sorting.type')
    },
    flags: {
        TUTORIAL: true,
        OPTIMISTIC_RENDERING: true // Assume writes are successful in order to render changes faster
    },
    katex: {
        throwOnError: true,
        displayMode: false,
        errorColor: '#f44336',
        delimilters: [
            { left: '¨D¨D', right: '¨D¨D', display: true },
            { left: '\\(', right: '\\)', display: false },
            { left: '\\[', right: '\\]', display: true },
            { left: '~', right: '~', display: false, asciimath: true },
            { left: '&&', right: '&&', display: true, asciimath: true }
        ]
    },
    mermaid: {}
};
/* EXPORT */
exports.default = Config;
